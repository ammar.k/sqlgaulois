<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
<style>

table, th, td {
    border: 1px solid black;
    border-collapse: collapse;
    width: 50px;
    color: black;
    font-family: monospace;
    font-size: 20px;
    text-align: center;
    }
</style>

    


<?php

// On se connecte à la base de données
// Souvent on identifie cet objet par la variable $conn ou $db
try
{
$mysqlConnection = new PDO(
    'mysql:host=localhost;dbname=gaulois;charset=utf8',
    'root',
    'root'
);
}
// On attrape les exceptions (erreur) si une exception est levée
//sinon le mot de pass sera affiché

catch (Exception $e)
{
        die('Erreur : ' . $e->getMessage());
}

// On récupère les personnages
$sqlQuery = "SELECT nom_personnage FROM personnage WHERE id_lieu = 1";
$result = $mysqlConnection->prepare($sqlQuery);
$result->execute();
$personnages = $result->fetchAll();


echo "<table>",
"<thead>",

"<tr>",
"<th>Personnages</th>",
"</tr>",
"</thead>",
"<tbody>";

foreach ($personnages as $personnage) {
    $tab = "
    <tr>
    <td>".$personnage['nom_personnage']."</td>
    </tr>";

    echo $tab;
}


?>

</body>
</html>