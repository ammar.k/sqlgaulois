/* 1 - Nom des lieux qui finissent par 'um'*/
SELECT nom_lieu
FROM lieu
WHERE nom_lieu LIKE '%um';

/* 2 - Nombre de personnages par lieu (trié par nombre de personnages décroissant)*/

select COUNT(id_personnage) as number_personnage, nom_lieu 
FROM personnage 
INNER JOIN lieu ON lieu.id_lieu=personnage.id_lieu 
GROUP BY nom_lieu 
ORDER BY number_personnage DESC;


 /* 3 - Nom des personnages + spécialité + adresse et lieu d'habitation, triés par lieu puis par nom de personnage.*/

 SELECT nom_personnage, adresse_personnage, nom_specialite, nom_lieu 
 FROM personnage, specialite,lieu 
 order by nom_lieu;

 /* 4 - Nom des spécialités avec nombre de personnages par spécialité (trié par nombre de personnages décroissant).*/

SELECT COUNT(id_personnage) as number_personnage, nom_specialite 
FROM personnage 
INNER JOIN specialite ON specialite.id_specialite=personnage.id_specialite
GROUP BY nom_specialite 
ORDER BY number_personnage DESC;

/* 5 -  Nom, date et lieu des batailles, classées de la plus récente à la plus ancienne (dates affichées au format jj/mm/aaaa). */

SELECT nom_bataille, DATE_FORMAT(date_bataille,"%d/%m/%Y"), nom_lieu
FROM bataille
INNER JOIN lieu ON lieu.id_lieu=bataille.id_lieu
ORDER BY date_bataille DESC;

/* 6 -  Nom des potions + coût de réalisation de la potion (trié par coût décroissant). */

SELECT nom_potion, SUM(composer.qte * ingredient.cout_ingredient) AS prix 
FROM potion 
INNER JOIN composer ON composer.id_potion=potion.id_potion 
INNER JOIN ingredient ON ingredient.id_ingredient=composer.id_ingredient 
GROUP BY potion.nom_potion 
ORDER BY prix DESC;

/* 7 - Nom des ingrédients + coût + quantité de chaque ingrédient qui composent la potion 'Santé'.*/

SELECT nom_ingredient, composer.qte , ingredient.cout_ingredient
FROM ingredient 
INNER JOIN composer ON composer.id_ingredient=ingredient.id_ingredient 
INNER JOIN potion on composer.id_potion=potion.id_potion 
WHERE potion.nom_potion LIKE 'Santé';

/* 8 - Nom du ou des personnages qui ont pris le plus de casques dans la bataille 'Bataille du village gaulois'.*/

SELECT nom_personnage,SUM(prendre_casque.qte) AS total 
FROM personnage
INNER JOIN prendre_casque ON prendre_casque.id_personnage=personnage.id_personnage 
WHERE prendre_casque.id_bataille = 1 
GROUP BY personnage.nom_personnage 
HAVING total >= ALL( 
    SELECT SUM(prendre_casque.qte) 
    FROM prendre_casque 
    INNER JOIN personnage ON personnage.id_personnage=prendre_casque.id_personnage 
    WHERE prendre_casque.id_bataille = 1 
    GROUP BY personnage.nom_personnage);

/* 9 - Nom des personnages et leur quantité de potion bue (en les classant du plus grand buveur au plus petit).*/

SELECT nom_personnage, SUM(boire.dose_boire) AS total
FROM personnage
INNER JOIN boire on boire.id_personnage=personnage.id_personnage
GROUP BY personnage.nom_personnage
ORDER BY total desc;


/* 10 - Nom de la bataille où le nombre de casques pris a été le plus important.*/

SELECT nom_bataille, SUM(prendre_casque.qte) AS total 
FROM bataille 
INNER JOIN prendre_casque ON prendre_casque.id_bataille=bataille.id_bataille 
GROUP BY bataille.nom_bataille 
HAVING total >= ALL( 
    SELECT SUM(prendre_casque.qte) 
    FROM prendre_casque 
    INNER JOIN bataille ON bataille.id_bataille=prendre_casque.id_bataille 
    GROUP BY bataille.id_bataille);

/* 11 - Combien existe-t-il de casques de chaque type et quel est leur coût total ? (classés par nombre décroissant).*/

SELECT nom_type_casque, COUNT(casque.id_type_casque) AS somme, SUM(casque.cout_casque) AS cout 
FROM type_casque 
INNER JOIN casque ON type_casque.id_type_casque=casque.id_type_casque 
GROUP BY casque.id_type_casque 
ORDER BY cout DESC;

/* 12 - Nom des potions dont un des ingrédients est le poisson frais.*/

SELECT nom_potion
FROM potion
INNER JOIN composer ON composer.id_potion=potion.id_potion
INNER JOIN ingredient ON ingredient.id_ingredient=composer.id_ingredient
WHERE ingredient.nom_ingredient LIKE 'Poisson frais';

/* 13 - Nom du / des lieu(x) possédant le plus d'habitants, en dehors du village gaulois.*/

SELECT nom_lieu, COUNT(personnage.nom_personnage) AS habitant 
FROM lieu INNER JOIN personnage ON lieu.id_lieu=personnage.id_lieu 
WHERE lieu.nom_lieu NOT LIKE 'Village gaulois' 
GROUP by lieu.nom_lieu 
HAVING habitant >= ALL( 
    SELECT COUNT(personnage.nom_personnage) 
    FROM personnage 
    INNER JOIN lieu 
    ON lieu.id_lieu=personnage.id_lieu 
    WHERE lieu.nom_lieu NOT LIKE 'Village gaulois' 
    GROUP by lieu.nom_lieu);

/* 14 - Nom des personnages qui n'ont jamais bu aucune potion */

SELECT nom_personnage 
FROM personnage 
LEFT JOIN boire ON boire.id_personnage=personnage.id_personnage 
WHERE boire.id_potion IS NULL;

/* 15 - Nom du / des personnages qui n'ont pas le droit de boire de la potion 'Magique'.*/

SELECT nom_personnage 
FROM personnage 
INNER JOIN autoriser_boire ON personnage.id_personnage=autoriser_boire.id_personnage 
WHERE NOT autoriser_boire.id_potion = 1;


/* deuxieme partie */

/* 1 - Ajoutez le personnage suivant : Champdeblix, agriculteur résidant à la ferme Hantassion de Rotomagus.*/

INSERT INTO personnage (nom_personnage, adresse_personnage, id_specialite, id_lieu)
VALUES ('Champdeblix', 'la ferme Hantassion',
            (SELECT id_specialite from specialite where nom_specialite = 'Agriculteur'),
            (SELECT id_lieu from lieu where nom_lieu = 'Rotomagus'));


/* 2 - Autorisez Bonemine à boire de la potion magique, elle est jalouse d'Iélosubmarine...*/

INSERT INTO autoriser_boire (id_personnage, id_potion)
VALUES ((SELECT id_personnage from personnage where nom_personnage = 'Bonemine'),
            (SELECT id_potion from potion where nom_potion = 'Magique'));

/* 3 - Supprimez les casques grecs qui n'ont jamais été pris lors d'une bataille.*/

DELETE FROM casque
WHERE id_type_casque = (SELECT id_type_casque from type_casque where nom_type_casque = 'Grec')
AND id_casque NOT IN (SELECT id_casque from prendre_casque);


/* 4 - Modifiez l'adresse de Zérozérosix : il a été mis en prison à Condate.*/

UPDATE personnage
SET adresse_personnage = 'prison',
    id_lieu = (SELECT id_lieu from lieu where nom_lieu = 'Condate')
WHERE nom_personnage = 'Zérozérosix';

/* 5 - La potion 'Soupe' ne doit plus contenir de persil*/

DELETE FROM composer
WHERE id_potion = (SELECT id_potion from potion where nom_potion = 'Soupe')
AND id_ingredient = (SELECT id_ingredient from ingredient where nom_ingredient = 'Persil');

/* 6 - Obélix s'est trompé : ce sont 42 casques Weisenau, et non Ostrogoths, qu'il a pris lors 
de la bataille 'Attaque de la banque postale'. Corrigez son erreur !*/

UPDATE prendre_casque
SET id_casque = (SELECT id_casque FROM casque WHERE nom_casque = 'Weisenau') ,qte = 42
WHERE id_personnage = (SELECT id_personnage from personnage where nom_personnage = 'Obélix')
AND id_bataille = (SELECT id_bataille from bataille where nom_bataille = 'Attaque de la banque postale');






